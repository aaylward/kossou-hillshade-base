#===============================================================================
# __init__.py
#===============================================================================

"""Plot hillshade from geospatial elevation TIFF file
"""

from kossou_hillshade_base.kossou_hillshade_base import (
    AZIMUTH,
    ALTITUDE,
    WIDTH,
    HEIGHT,
    REPORT,
    DEFAULT_ELEVATION_TIFF,
    DEFAULT_COLORMAP_ELEVATION,
    DEFAULT_COLORMAP_HILLSHADE,
    plot_elevation,
    plot_hillshade,
    generate_report
)
